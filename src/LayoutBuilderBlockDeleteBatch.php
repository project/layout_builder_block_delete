<?php

namespace Drupal\layout_builder_block_delete;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides batch processing for deleting blocks from nodes.
 */
class LayoutBuilderBlockDeleteBatch implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The layout builder block delete manager.
   *
   * @var \Drupal\layout_builder_block_delete\LayoutBuilderBlockDeleteManager
   */
  protected $layoutBuilderBlockDeleteManager;

  /**
   * Constructs a new LayoutBuilderBlockDeleteBatch object.
   */
  public function __construct(LayoutBuilderBlockDeleteManager $layout_builder_block_delete_manager) {
    $this->layoutBuilderBlockDeleteManager = $layout_builder_block_delete_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('layout_builder_block_delete.manager')
    );
  }

  /**
   * Kick off batch process to delete blocks on all nodes for content types.
   *
   * @param array $content_types
   *   The content types.
   * @param string|null $block_type
   *   The block type.
   */
  public function batchStart(array $content_types, string $block_type = NULL) {
    $nodes = [];

    foreach ($content_types as $content_type) {
      if ($content_type) {
        $nodes += $this->layoutBuilderBlockDeleteManager->getNodesForContentType($content_type);
      }
    }

    if (count($nodes)) {
      batch_set([
        'title' => $this->t('Deleting blocks'),
        'init_message' => $this->t('Starting blocks deletion'),
        'operations' => [
          [
            [
              'Drupal\layout_builder_block_delete\LayoutBuilderBlockDeleteBatch',
              'batchDeleteAllBlocks',
            ],
            [
              ['nids' => array_keys($nodes), 'block_type' => $block_type],
            ],
          ],
        ],
        'finished' => [
          'Drupal\layout_builder_block_delete\LayoutBuilderBlockDeleteBatch',
          'batchCompleted',
        ],
      ]);
    }
  }

  /**
   * Load nodes in batch process progressively to delete all blocks.
   *
   * @param array $data
   *   Additional parameters specific to the batch. These are specified in the
   *   array passed to batch_set().
   * @param array $context
   *   The batch context array, passed by reference.
   */
  public static function batchDeleteAllBlocks(array $data, array &$context) {
    $nids = $data['nids'];

    if (empty($context['sandbox'])) {
      // Flush caches to avoid false positives looking for block UUID.
      drupal_flush_all_caches();
      $context['sandbox'] = [];
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['current_node'] = 0;
      // Save node count for the termination message.
      $context['sandbox']['max'] = count($nids);
    }

    $limit = 5;

    // Retrieve the next group.
    $current_node = $context['sandbox']['current_node'];
    $limit_node = $context['sandbox']['current_node'] + $limit;
    $result = range($current_node, $limit_node);

    foreach ($result as $row) {
      /** @var \Drupal\layout_builder_block_delete\LayoutBuilderBlockDeleteManager $layout_builder_block_delete_manager */
      $layout_builder_block_delete_manager = \Drupal::service('layout_builder_block_delete.manager');

      if (!empty($nids[$row])) {
        $layout_builder_block_delete_manager->deleteBlocks($nids[$row], $data['block_type']);
      }

      $operation_details = t('Deleting blocks for nodes :current to :limit', [
        ':current' => $current_node,
        ':limit' => $limit_node,
      ]);

      // Store some results for post-processing in the 'finished' callback.
      // The contents of 'results' will be available as $results in the
      // 'finished' function.
      $context['results'][] = $row;

      // Update our progress information.
      $context['sandbox']['progress']++;
      $context['sandbox']['current_node'] = $row;
      $context['message'] = t('@details', [
        '@details' => $operation_details,
      ]);
    }

    // Inform the batch engine that we are not finished,
    // and provide an estimation of the completion level we reached.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * Callback for batch processing completed.
   *
   * @param bool $success
   *   A boolean indicating whether the batch has completed successfully.
   * @param array $results
   *   The value set in $context['results'] by callback_batch_operation().
   * @param array $operations
   *   If $success is FALSE, contains the operations that remained unprocessed.
   * @param string $elapsed
   *   A string representing the elapsed time for the batch process, e.g.,
   *   '1 min 30 secs'.
   */
  public static function batchCompleted(bool $success, array $results, array $operations, string $elapsed) {
    $messenger = \Drupal::messenger();
    if ($success) {
      $messenger->addMessage(t('@count nodes were processed.', [
        '@count' => count($results),
      ]));
    }
    else {
      // An error occurred.
      $messenger->addMessage(t('An error occurred while processing.'));
    }
  }

}
