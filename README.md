# Layout Builder Block Delete

This module aims to provide a way to automatically delete blocks from nodes when
you've placed those blocks on a Layout Builder based display for a node content
type.

The module provides an EventSubscriber that allows to override the node list per 
content type. This could for example be very helpful if you need to get all 
nodes from a migration and delete its Layout Builder blocks.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/layout_builder_block_delete).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/layout_builder_block_delete).


## Table of contents

- Requirements
- Installation
- Configuration/Usage
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration/Usage

1. Navigate to Administration > System > Layout Builder Block Delete.
2. Select for which content types you would like to delete blocks from the nodes.


## Maintainers

- Tim Diels - [tim-diels](https://www.drupal.org/u/tim-diels)
- Mathieu De Meue - [matdemeue](https://www.drupal.org/u/matdemeue)
