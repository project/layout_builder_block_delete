<?php

namespace Drupal\layout_builder_block_delete\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Allow other modules to override the nodes for content type.
 */
class OverrideNodesForContentType extends Event {

  const EVENT_NAME = 'layout_builder_block_delete.override_nodes_for_content_type';

  /**
   * The nodes.
   *
   * @var array
   */
  protected $nodes;

  /**
   * The content type.
   *
   * @var string
   */
  protected $contentType;

  /**
   * OverrideNodesForContentType constructor.
   *
   * @param array $nodes
   *   The nodes.
   * @param string $content_type
   *   The content type.
   */
  public function __construct(array $nodes, string $content_type) {
    $this->nodes = $nodes;
    $this->contentType = $content_type;
  }

  /**
   * Get the nodes.
   *
   * @return array
   *   The nodes.
   */
  public function getNodes(): array {
    return $this->nodes;
  }

  /**
   * Get the content type.
   *
   * @return string
   *   The content type.
   */
  public function getContentType(): string {
    return $this->contentType;
  }

  /**
   * Set the nodes.
   *
   * @param array $nodes
   *   The nodes.
   *
   * @return \Drupal\layout_builder_block_delete\Event\OverrideNodesForContentType
   *   The updated event.
   */
  public function setNodes(array $nodes): OverrideNodesForContentType {
    $this->nodes = $nodes;
    return $this;
  }

}
