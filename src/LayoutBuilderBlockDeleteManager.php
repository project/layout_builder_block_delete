<?php

namespace Drupal\layout_builder_block_delete;

use Drupal\block_content\BlockContentUuidLookup;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Error;
use Drupal\layout_builder\LayoutEntityHelperTrait;
use Drupal\layout_builder\LayoutTempstoreRepository;
use Drupal\layout_builder\SectionStorage\SectionStorageManager;
use Drupal\layout_builder_block_delete\Event\OverrideNodesForContentType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Provides helper methods that are used throughout the module.
 */
class LayoutBuilderBlockDeleteManager implements ContainerInjectionInterface {

  use LayoutEntityHelperTrait;
  use StringTranslationTrait;

  /**
   * Drupal\block_content\BlockContentUuidLookup definition.
   *
   * @var \Drupal\block_content\BlockContentUuidLookup
   */
  protected $blockContentUuidLookup;

  /**
   * Drupal\layout_builder\SectionStorage\SectionStorageManager definition.
   *
   * @var \Drupal\layout_builder\SectionStorage\SectionStorageManager
   */
  protected $pluginManagerLayoutBuilderSectionStorage;

  /**
   * Drupal\layout_builder\LayoutTempstoreRepository definition.
   *
   * @var \Drupal\layout_builder\LayoutTempstoreRepository
   */
  protected $layoutBuilderTempstoreRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The logger for this channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Constructs a new LayoutBuilderBlockSanitizerManager object.
   *
   * @param \Drupal\block_content\BlockContentUuidLookup $block_content_uuid_lookup
   *   The block content uuid lookup.
   * @param \Drupal\layout_builder\SectionStorage\SectionStorageManager $plugin_manager_layout_builder_section_storage
   *   The plugin manager layout builder section storage.
   * @param \Drupal\layout_builder\LayoutTempstoreRepository $layout_builder_tempstore_repository
   *   The layout builder tempstore repository.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger for this channel.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   */
  public function __construct(BlockContentUuidLookup $block_content_uuid_lookup, SectionStorageManager $plugin_manager_layout_builder_section_storage, LayoutTempstoreRepository $layout_builder_tempstore_repository, EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, EventDispatcherInterface $event_dispatcher, LoggerChannelFactoryInterface $logger_factory, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->blockContentUuidLookup = $block_content_uuid_lookup;
    $this->pluginManagerLayoutBuilderSectionStorage = $plugin_manager_layout_builder_section_storage;
    $this->layoutBuilderTempstoreRepository = $layout_builder_tempstore_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->eventDispatcher = $event_dispatcher;
    $this->logger = $logger_factory->get('layout_builder_block_delete');
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('block_content.uuid_lookup'),
      $container->get('plugin.manager.layout_builder.section_storage'),
      $container->get('layout_builder.tempstore_repository'),
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('event_dispatcher'),
      $container->get('logger.factory'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * Returns a list of all the available block types.
   *
   * @return array
   *   An array of block types.
   */
  public function getBlockTypesAsOptions(): array {
    $block_types = [];

    $bundles = $this->entityTypeBundleInfo->getBundleInfo('block_content');
    foreach ($bundles as $machine_name => $bundle) {
      $block_types[$machine_name] = $bundle['label'];
    }

    return $block_types;
  }

  /**
   * Returns a list of all the content types currently installed.
   *
   * @return array
   *   An array of content types.
   */
  public function getContentTypesAsOptions(): array {
    $content_types = [];

    /** @var \Drupal\node\NodeTypeInterface $node_type */
    foreach ($this->entityTypeManager->getStorage('node_type')->loadMultiple() as $node_type) {

      // Get only Layout Builder enabled node types.
      $storage = $this->entityTypeManager->getStorage('entity_view_display');
      $view_display = $storage->load('node.' . $node_type->id() . '.full');
      if (!is_null($view_display)) {
        $layout_builder_enabled = $view_display->getThirdPartySetting('layout_builder', 'enabled');
        if ($layout_builder_enabled) {
          $content_types[$node_type->id()] = $node_type->label();
        }
      }
    }

    asort($content_types);

    return $content_types;
  }

  /**
   * Returns a list of all nodes for a content type.
   *
   * @param string $content_type
   *   The content type.
   *
   * @return array
   *   An array of node IDs.
   */
  public function getNodesForContentType(string $content_type): array {
    $properties = ['type' => $content_type];
    $nodes = $this->entityTypeManager->getStorage('node')->loadByProperties($properties);

    if (empty($nodes)) {
      $this->messenger->addWarning($this->t('There are no nodes to process.'));
    }

    // Allow other modules to alter the nodes list.
    $overrideNodesForContentType = new OverrideNodesForContentType($nodes, $content_type);
    $this->eventDispatcher->dispatch($overrideNodesForContentType, OverrideNodesForContentType::EVENT_NAME);

    return $overrideNodesForContentType->getNodes();
  }

  /**
   * Delete all blocks for a node.
   *
   * @param int $nid
   *   The node ID.
   * @param string|null $block_type
   *   The block type.
   */
  public function deleteBlocks(int $nid, string $block_type = NULL) {
    try {
      $entity = $this->entityTypeManager->getStorage('node')->load($nid);
      $section_storage = $this->getSectionStorageForEntity($entity);
      $sections = $section_storage->getSections();
      foreach ($sections as &$section) {
        $components = $section->getComponents();
        if (!empty($components)) {
          foreach ($components as $section_component_uuid => $section_component) {
            if (!empty($block_type)) {
              if ($section_component->getPluginId() === 'inline_block:' . $block_type) {
                $section->removeComponent($section_component_uuid);
              }
            }
            else {
              $section->removeComponent($section_component_uuid);
            }
          }
        }
      }
      $section_storage->save();
    }
    catch (\Exception $e) {
      $this->messenger->addWarning($this->t("An exception was encountered: :e", [':e' => $e->getMessage()]));
      $this->logger->error(Error::DEFAULT_ERROR_MESSAGE, Error::decodeException($e));
    }
  }

}
