<?php

namespace Drupal\layout_builder_block_delete\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder_block_delete\LayoutBuilderBlockDeleteBatch;
use Drupal\layout_builder_block_delete\LayoutBuilderBlockDeleteManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Layout builder block delete form.
 */
class DeleteForm extends FormBase implements ContainerInjectionInterface {

  /**
   * The layout builder block delete batch service.
   *
   * @var \Drupal\layout_builder_block_delete\LayoutBuilderBlockDeleteBatch
   */
  protected $layoutBuilderBlockDeleteBatch;

  /**
   * The layout builder block delete manager.
   *
   * @var \Drupal\layout_builder_block_delete\LayoutBuilderBlockDeleteManager
   */
  protected $layoutBuilderBlockDeleteManager;

  /**
   * Constructs a DeleteForm object.
   *
   * @param \Drupal\layout_builder_block_delete\LayoutBuilderBlockDeleteBatch $layout_builder_block_delete_batch
   *   The layout builder block delete batch service.
   * @param \Drupal\layout_builder_block_delete\LayoutBuilderBlockDeleteManager $layout_builder_block_delete_manager
   *   The layout builder block delete manager.
   */
  public function __construct(LayoutBuilderBlockDeleteBatch $layout_builder_block_delete_batch, LayoutBuilderBlockDeleteManager $layout_builder_block_delete_manager) {
    $this->layoutBuilderBlockDeleteBatch = $layout_builder_block_delete_batch;
    $this->layoutBuilderBlockDeleteManager = $layout_builder_block_delete_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('layout_builder_block_delete.batch'),
      $container->get('layout_builder_block_delete.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'layout_builder_block_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['block_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Block type'),
      '#options' => $this->layoutBuilderBlockDeleteManager->getBlockTypesAsOptions(),
      '#empty_option' => $this->t('- All -'),
      '#description' => $this->t('You can optionally select a block type. If none is selected, all blocks will be deleted.'),
    ];
    $form['content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content types'),
      '#options' => $this->layoutBuilderBlockDeleteManager->getContentTypesAsOptions(),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Delete blocks',
      '#description' => $this->t('Delete all blocks from Layout Builder enabled nodes for the selected content types.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $content_types = $form_state->getValue('content_types');
    $block_type = $form_state->getValue('block_type');
    $this->layoutBuilderBlockDeleteBatch->batchStart($content_types, $block_type);
  }

}
